﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using System;
using System.Collections.ObjectModel;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Linq;
using System.Runtime.Loader;
using System.Text;
using System.Timers;
using System.Transactions;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private ITimerService WithdrawTimer { get; set; }
        private ITimerService LogTimer { get; set; }
        private ILogService LogService { get; }

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            WithdrawTimer = withdrawTimer;
            WithdrawTimer.Elapsed += Charge;
            WithdrawTimer.Start();
            LogTimer = logTimer;
            LogTimer.Elapsed += RemoveOldTransaction;
            LogTimer.Start();
            LogService = logService;
        }
        public decimal GetBalance()
        {
            return Parking.Instance.Balance;
        }

        public int GetCapacity()
        {
            return Parking.Instance.Capacity;
        }

        public int GetFreePlaces()
        {
            return Parking.Instance.Capacity - Parking.Instance.Vehicles.Count;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            var readOnlyCollection = new ReadOnlyCollection<Vehicle>(Parking.Instance.Vehicles);
            return readOnlyCollection;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (GetFreePlaces() == 0) throw new InvalidOperationException();
            if ((Parking.Instance.Vehicles.Any(v => v.Id == vehicle.Id))) throw new ArgumentException();
            Parking.Instance.Vehicles.Add(vehicle);
        }

        public void RemoveVehicle(string vehicleId)
        {
            if (Parking.Instance.Vehicles.Any(v => v.Id == vehicleId))
            {
                var vehicle = Parking.Instance.Vehicles.First(v => v.Id == vehicleId);
                if (vehicle.Balance < 0) throw new InvalidOperationException();
                Parking.Instance.Vehicles.Remove(vehicle);
            }
            else throw new ArgumentException();
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (Parking.Instance.Vehicles.Any(v => v.Id == vehicleId) && (sum > 0))
            {
                var vehicle = Parking.Instance.Vehicles.First(v => v.Id == vehicleId);
                vehicle.Balance += sum;
            }
            else throw new ArgumentException();
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return Parking.Instance.Transactions.ToArray();
        }

        public string ReadFromLog()
        {
            return LogService.Read();
        }
        
        public void Dispose()
        {
            Parking.Instance.Vehicles.Clear();
            Parking.Instance.Transactions.Clear();
            Parking.Instance.Balance = Settings.InitialBalance;
        }

        private void Charge(object source, ElapsedEventArgs e)
        {
            foreach (var vehicle in Parking.Instance.Vehicles)
            {
                var price = Settings.PriceList[vehicle.VehicleType];
                if (vehicle.Balance < price)
                {
                    if (vehicle.Balance > 0) price = (price - vehicle.Balance) * Settings.Fine + vehicle.Balance;
                    else
                    {
                        price *= Settings.Fine;
                    }
                }

                vehicle.Balance -= price;
                Parking.Instance.Balance += price;
                Parking.Instance.Transactions.Add(new TransactionInfo(Vehicle.GenerateRandomRegistrationPlateNumber(),
                    DateTime.Now, price));

            }
        }
        private void RemoveOldTransaction(object source, ElapsedEventArgs e)
        {
            StringBuilder accumulator = new StringBuilder();
            foreach (var transaction in Parking.Instance.Transactions)
            {
                accumulator.Append("Identifier: " + transaction.VehicleId.ToString() + " DateTime: " + 
                                   transaction.TransactionDate.ToString() +  " Transaction Amount: " + 
                                   transaction.Sum.ToString() + "\n");
                
            }
            LogService.Write(accumulator.ToString());
            Parking.Instance.Transactions.Clear();
        }
    }
}