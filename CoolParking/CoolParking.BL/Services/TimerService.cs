﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using System.Timers;
using System.Transactions;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        public TimerService(double interval)
        {
            Interval = interval;
        }

        public event ElapsedEventHandler Elapsed;
        private Timer _timer = new Timer();
        public double Interval { get; set; }
        public void Start()
        {
            _timer.Interval = Interval; 
            _timer.Start();
            _timer.AutoReset = true;
            _timer.Enabled = true;
        }
        public void Stop()
        {
            _timer.Stop();
        }
        public void Dispose()
        {
            
        }
        
    }
}