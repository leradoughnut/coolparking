﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using System;
using System.IO;
using System.Text;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public LogService(string logFilePath)
        {
            LogPath = logFilePath;
        }

        public string LogPath { get; }

        public void Write(string logInfo)
        {
            using (StreamWriter sw = new StreamWriter(LogPath, true, System.Text.Encoding.Default))
            {
                sw.WriteLine(logInfo);
            }
        }

        public string Read()
        {
            if (!File.Exists(LogPath)) throw new InvalidOperationException();
            using (StreamReader sr = new StreamReader(LogPath, System.Text.Encoding.Default))
            {
                StringBuilder accumulator = new StringBuilder();
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    accumulator.Append(line + "\n");
                }
                return accumulator.ToString();
            };
        }
    }
}