﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Transactions;

namespace CoolParking.BL.Models
{
    public sealed class Parking
    {
        private static readonly Lazy<Parking> LazyParking = new Lazy<Parking>(() => new Parking());
        public static Parking Instance => LazyParking.Value;
        public List<Vehicle> Vehicles { get; }
        public List<TransactionInfo> Transactions { get; }
        public decimal Balance { get; set; }
        public int Capacity { get; set; }

        private Parking()
        {
            Vehicles = new List<Vehicle>();
            Transactions = new List<TransactionInfo>();
            Balance = Settings.InitialBalance;
            Capacity = Settings.ParkingSpace;
        }
    }
}